﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Beanworks.SyncTool.SyncService.Logging {
    public class LoggerService : ILogger {
        public void Log<T>(LogLevels level, T logEvent) {
            Console.WriteLine($"{level}: {logEvent}");
        }
    }
}
