﻿namespace Beanworks.SyncTool.SyncService {
    public static class LoggerUtilities {
        public static void LogInfo<T>(this ILogger logger, T logEvent) {
            logger.Log(LogLevels.Info, logEvent);
        }
        public static void LogError<T>(this ILogger logger, T logEvent) {
            logger.Log(LogLevels.Error, logEvent);
        }
    }
}