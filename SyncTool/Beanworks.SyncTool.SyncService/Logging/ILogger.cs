﻿namespace Beanworks.SyncTool.SyncService {
    public interface ILogger {
        void Log<T>(LogLevels level, T logEvent);
    }
}