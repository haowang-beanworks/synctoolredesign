﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Beanworks.SyncTool.SyncService.Entities {
    public class Batch {
        public IList<PoInvoice> PoInvoices { get; set; }
        public IList<ApInvoice> ApInvoices { get; set; }
        public IList<PurchaseOrder> PurchaseOrders { get; set; }
        public IList<Payment> Payments { get; set; }
    }
}
