﻿using System.Web.Http;

namespace Beanworks.SyncTool.SyncService {
    public class SyncController : ApiController {
        private SyncProcessor SyncProcessor;

        public SyncController(SyncProcessor syncProcessor) {
            SyncProcessor = syncProcessor;
        }

        public void Post(SyncCommand syncCommand) {
            SyncProcessor.Execute(syncCommand);
        }
    }
}