﻿namespace Beanworks.SyncTool.SyncService {
    public interface IItemProcessor<T> {
        void Execute(T item);
    }
}