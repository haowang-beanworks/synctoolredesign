﻿using System.Collections.Generic;
using System.IO;
using Beanworks.SyncTool.SyncService.Entities;

namespace Beanworks.SyncTool.SyncService {
    public class ImportProcessor {
        private ListProcessor<ApInvoice> apInvoiceListProcessor;
        private ListProcessor<PoInvoice> poInvoiceProcessor;
        private ILogger logger;

        public ImportProcessor(ILogger logger, ListProcessor<ApInvoice> apInvoiceListProcessor, ListProcessor<PoInvoice> poInvoiceProcessor) {
            this.logger = logger;
            this.apInvoiceListProcessor = apInvoiceListProcessor;
            this.poInvoiceProcessor = poInvoiceProcessor;
        }

        public void Execute() {
            logger.LogInfo("Import Started");
            //Download from SFTP
            //Loop each files
            var files = new List<FileInfo> { new FileInfo(@"C:\Imports\F1.json") };
            foreach (var file in files) {
                //Parse/Deserilize File  
                var Batch = new Batch {
                    ApInvoices = new List<ApInvoice> { new ApInvoice { Number = "AP001" }, new ApInvoice { Number = "AP002" }, new ApInvoice { Number = "ERROR" } },
                    PoInvoices = new List<PoInvoice> { new PoInvoice { Number = "PO001" }, new PoInvoice { Number = "PO002" }, new PoInvoice { Number = "ERROR" } }
                };
                apInvoiceListProcessor.Execute(Batch.ApInvoices);
                poInvoiceProcessor.Execute(Batch.PoInvoices);
            }

            logger.LogInfo("Import Finished.");
        }
    }
}