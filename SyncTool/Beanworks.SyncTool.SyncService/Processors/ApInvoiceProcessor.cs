﻿using System;
using Beanworks.SyncTool.SyncService.Entities;

namespace Beanworks.SyncTool.SyncService {
    public class ApInvoiceProcessor : IItemProcessor<ApInvoice> {
        public void Execute(ApInvoice item) {
            //Save ApInvoice
            if (item.Number == "ERROR") {
                throw new Exception("Processing the item xxx error. ");

            }
            Console.WriteLine($"Importing AP Invoice {item.Number}");

        }
    }
}