﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Beanworks.SyncTool.SyncService {
    public class ListProcessor<T> {
        private ILogger logger;
        private IItemProcessor<T> itemProcessor;

        public ListProcessor(IItemProcessor<T> itemProcessor, ILogger logger) {
            this.itemProcessor = itemProcessor;
            this.logger = logger;
        }

        public void Execute(IEnumerable<T> list) {
            logger.LogInfo($"List {typeof(T).Name} Started.");
            var total = list.Count();
            var successRecords = 0;
            var errors = new List<string>();
            foreach (var item in list) {
                try {
                    itemProcessor.Execute(item);
                    successRecords++;
                } catch (Exception e) {
                    errors.Add(e.ToString());
                }
            }
            logger.LogInfo($"The {successRecords} have been processed of the total {total} ");
            logger.LogError(string.Join(Environment.NewLine, errors));
            logger.LogInfo($"List {typeof(T).Name} Finished.");
        }
    }
}