﻿using System;

namespace Beanworks.SyncTool.SyncService {
    public class SyncCommand {
        public Guid Id { get; set; }
        public DateTime? LastSyncDate { get; set; }
        public bool PoReceivingSupportEnabled { get; set; }
        public DateTime? PoReceivingSupportStartSyncDate { get; set; }
        public Guid LegalEntityId { get; set; }
    }
}