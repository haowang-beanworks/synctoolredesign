﻿using System;

namespace Beanworks.SyncTool.SyncService {
    public class SyncProcessor {
        private ILogger logger;
        private ImportProcessor importProcessor;
        private ExportProcessor exportProcessor;

        public SyncProcessor(ImportProcessor importProcessor, ExportProcessor exportProcessor, ILogger logger) {
            this.importProcessor = importProcessor;
            this.exportProcessor = exportProcessor;
            this.logger = logger;
        }

        public void Execute(SyncCommand command) {
            logger.LogInfo("Sync Started");
            try {
                importProcessor.Execute();
                exportProcessor.Execute();
            } catch (Exception e) {
                logger.LogError(e);
            }

            logger.LogInfo("Sync Finished.");
        }
    }
}