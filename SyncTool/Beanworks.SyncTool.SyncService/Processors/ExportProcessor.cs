﻿namespace Beanworks.SyncTool.SyncService {
    public class ExportProcessor {
        private ILogger logger;

        public ExportProcessor(ILogger logger) {
            this.logger = logger;
        }

        public void Execute() {
            logger.LogInfo("Export Started");


            logger.LogInfo("Export Finished.");
        }
    }
}