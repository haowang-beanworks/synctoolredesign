﻿using System;
using Beanworks.SyncTool.SyncService.Entities;

namespace Beanworks.SyncTool.SyncService {
    public class PoInvoiceProcessor : IItemProcessor<PoInvoice> {
        public void Execute(PoInvoice item) {
            //Save 
            if (item.Number == "ERROR") {
                throw new Exception("Processing the item xxx error. ");

            }
            Console.WriteLine($"Importing PO Invoice {item.Number}");
        }
    }
}