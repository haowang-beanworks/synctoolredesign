﻿using System.Reflection;
using System.Web.Http;
using Autofac;
using Autofac.Integration.WebApi;
using Owin;
using Swashbuckle.Application;

namespace Beanworks.SyncTool.SyncService {
    public class StartUp {
        public void Configuration(IAppBuilder appBuilder) {
            // Configure Web API for self-host. 
            HttpConfiguration config = new HttpConfiguration();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(CreateContainer());
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}",
                defaults: new { id = RouteParameter.Optional }
            );
#if DEBUG
            config
                .EnableSwagger(c => c.SingleApiVersion("v1", "Sync Process API"))
                .EnableSwaggerUi();
#endif
            appBuilder.UseWebApi(config);
        }

        private IContainer CreateContainer() {
            var builder = new ContainerBuilder();
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly())
                .Where(t => t.Name.EndsWith("Service"))
                .AsImplementedInterfaces();
            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly())
                .Where(t => t.Name.EndsWith("Processor"))
                .AsSelf();
            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly())
                .AsClosedTypesOf(typeof(IItemProcessor<>)).AsImplementedInterfaces();
            builder.RegisterGeneric(typeof(ListProcessor<>)).AsSelf();
            return builder.Build();
        }
    }
}