﻿using System;
using Microsoft.Owin.Hosting;

namespace Beanworks.SyncTool.SyncService {
    public class Program {
        static void Main(string[] args) {
            var baseAddress = "http://localhost:59999/";
            WebApp.Start<StartUp>(baseAddress);
            Console.ReadLine();
        }
    }
}
